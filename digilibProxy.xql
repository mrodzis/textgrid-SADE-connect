xquery version "3.1";
module namespace digilib="http://textgrid.info/namespaces/xquery/digilib";


import module namespace console="http://exist-db.org/xquery/console";
import module namespace tgclient="http://textgrid.info/namespaces/xquery/tgclient" at "/db/apps/textgrid-connect/tgclient.xqm";
import module namespace config="http://exist-db.org/xquery/apps/config" at "../SADE/core/config.xqm";
import module namespace fconfig="https://fontane-nb.dariah.eu/fontane-config" at "../SADE/core/fconfig.xqm";

import module namespace datetime = "http://exist-db.org/xquery/datetime" at "java:org.exist.xquery.modules.datetime.DateTimeModule";

declare namespace tg="http://textgrid.info/namespaces/metadata/core/2010";
declare namespace digili="http://textgrid.info/namespaces/digilib";
declare namespace digil="digilib:digilib";

declare
    %rest:GET
    %rest:path("/digilib/{$project}/{$id}")
    %rest:header-param("if-modified-since", "{$if-modified-since}")
function digilib:proxy($project as xs:string, $id as xs:string*, $if-modified-since as xs:string*) {
if (contains(request:get-query-string() , 'm2'))
    then (
        let $reqUrl := xs:anyURI('http://localhost:8080/exist/apps/textgrid-connect/digilibProxyMiradorForward.xql?image='||$id)

        let $header := <headers><header name="Connection" value="close"/></headers>

        let $result := httpclient:get($reqUrl, false(), $header)

        let $mime := xs:string($result//httpclient:header[@name="Content-Type"]/@value)
        let $last-modified := xs:string($result//httpclient:header[@name="Last-Modified"]/@value)
        let $cache-control := xs:string($result//httpclient:header[@name="Cache-Control"]/@value)
        return
            (util:declare-option("exist:serialize", "method=xhtml media-type=text/html omit-xml-declaration=yes indent=yes") ,
            $result//httpclient:body/*)
    )
else
    let $query := if(request:get-parameter-names() = ('dw', 'dh')) then request:get-query-string()  else 'dh=1500&amp;'||request:get-query-string()
    let $config := map { "config" := config:config($project) }
    let $data-dir := fconfig:get('data-dir')

    let $id :=  if (matches($id, '_') and (starts-with($id, 'A') or starts-with($id, 'B') or starts-with($id, 'C') or starts-with($id, 'D') or starts-with($id, 'E')) )
                then collection('/db/sade-projects/' || $project )//digil:image[@name = $id]/string(@uri)[last()]
                else $id
(:        let $test := console:log($id):)
(: how to distinguish between public and non-public data? :)
(:        let $sid := tgclient:getSidCached($config):)
        let $sid := ""
        let $reqUrl := fconfig:get("textgrid.digilib") || "/"
        let $header := <headers><header name="Connection" value="close"/></headers>

        let $result := httpclient:get( xs:anyURI(concat($reqUrl,$id,";sid=",$sid,"?",$query)), false(), $header )

        let $mime := xs:string($result//httpclient:header[@name="Content-Type"]/@value)
        let $last-modified := xs:string($result//httpclient:header[@name="Last-Modified"]/@value)
        let $cache-control := xs:string($result//httpclient:header[@name="Cache-Control"]/@value)


        return(
            try { response:stream-binary(data($result//httpclient:body), $mime) }
            catch * { <error>Caught error {$err:code}: {$err:description}</error>}
)
};
