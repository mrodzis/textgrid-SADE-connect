xquery version "3.0";

module namespace tgconnect="http://textgrid.info/namespaces/xquery/tgconnect";

import module namespace templates="http://exist-db.org/xquery/templates";
import module namespace tgclient="http://textgrid.info/namespaces/xquery/tgclient" at "tgclient.xqm";
import module namespace req="http://exquery.org/ns/request";
import module namespace tgmenu="http://textgrid.info/namespaces/xquery/tgmenu" at "/db/apps/textgrid-connect/tgmenu.xqm";
import module namespace console="http://exist-db.org/xquery/console";
import module namespace fconfig="https://fontane-nb.dariah.eu/fontane-config" at "../SADE/core/fconfig.xqm";

import module namespace fontaneTransfo="http://fontane-nb.dariah.eu/Transfo"
  at '/db/apps/SADE/modules/multiviewer/fontane.xqm';
import module namespace fontaneSfEx="http://fontane-nb.dariah.eu/SfEx"
  at '/db/apps/SADE/modules/multiviewer/fontane-surfaceExtract.xqm';

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace rest="http://exquery.org/ns/restxq";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace http="http://expath.org/ns/http-client";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";
declare namespace xhtml="http://www.w3.org/1999/xhtml";
declare namespace svg="http://www.w3.org/2000/svg";

declare variable $tgconnect:websiteDataPath := "/data/website";
declare variable $tgconnect:editionDataPath := "/data/xml";

declare variable $tgconnect:config :=   let $project:= substring-before(
                                                substring-after(request:get-url(), "/publish/"),
                                                "/")
                                        let $projectConfigDoc := doc( "/db/sade-projects/" || $project || "/config.xml" )
                                    return
                                        map:new(
                                            for $param in $projectConfigDoc//param
                                            let $key := if ($param/ancestor::*[@key])
                                                        then string-join(($param/ancestor-or-self::*[@key]/string(@key)), "#")
                                                        else $param/@key
                                            return
                                                map:entry($key, ($param/node()))
                                            );

(:~
 : show html.
 :)
declare
    %rest:GET
    %rest:path("/publish/{$project}/")
    %output:media-type("text/html")
    %output:method("html5")
function tgconnect:page($project as xs:string*) {
    let $serverPort:= request:get-server-port()
    let $content := switch( $tgconnect:config("sade.develop") )
                    case ("true") return doc("publish-gui.html")
                    default return doc("publish-gui-LIVE.html")
    let $config := map {
        (: The following function will be called to look up template parameters :)
        $templates:CONFIG_PARAM_RESOLVER := function($param as xs:string) as xs:string* {
            req:parameter($param)
        }
    }
    let $lookup := function($functionName as xs:string, $arity as xs:int) {
        try {
            function-lookup(xs:QName($functionName), $arity)
        } catch * {
            ()
        }
    }

    return
        templates:apply($content, $lookup, (), $config)
};

(: pubblish function w/o login for inital publish process on autodeployment :)

declare
    %rest:POST
    %rest:path("/publish/{$project}/process/")
    %rest:form-param("sid", "{$sid}", "")
    %rest:form-param("uri", "{$uri}", "")
    %rest:form-param("target", "{$target}", "data")
    %rest:form-param("user", "{$user}", "")
    %rest:form-param("password", "{$password}", "")

(:    %rest:produces("text/plain"):)

function tgconnect:publish( $uri as xs:string,
                            $sid as xs:string,
                            $target as xs:string,
                            $user as xs:string,
                            $password as xs:string,
                            $project as xs:string) {

    tgconnect:publish( $uri,
                            $sid,
                            $target,
                            $user,
                            $password,
                            $project,
                            false())

};

(:
 : publish data to project
 :)
declare function tgconnect:publish( $uri as xs:string,
                            $sid as xs:string,
                            $target as xs:string,
                            $user as xs:string,
                            $password as xs:string,
                            $project as xs:string,
                            $login as xs:boolean) {

    let $config := tgclient:getConfig($project)

    let $targetPath :=
        if($target eq "website") then
            "/sade-projects/" || $project || $tgconnect:websiteDataPath
        else
            "/sade-projects/" || $project || $tgconnect:editionDataPath

    let $log := if( $login = false() ) then xmldb:login($targetPath, $user, $password ) else true()

    return

        if ( $log ) then
        let $tgcrudUrl :=  try { fconfig:get("textgrid.tgcrud") } catch * { "https://textgridlab.org/1.0/tgcrud/rest" }

        (: work around strange bug with publish from public repo
           where a .0 to much is added.
           TODO: research
        :)
        let $tguri := if(ends-with($uri, ".0.0")) then
                substring-before($uri, ".") || ".0"
            else
                $uri

        let $mp :=  tgclient:getMeta($tguri, $sid, $tgcrudUrl)
        let $tguri := string($mp//tgmd:textgridUri)

        let $rdfstoreUrl :=
            if ($mp//tgmd:generated/tgmd:availability = "public") then
                fconfig:get("textgrid.public-triplestore")
            else
                fconfig:get("textgrid.nonpublic-triplestore")
        let $egal := tgconnect:createEntryPoint($tguri, concat($targetPath, "/meta"))
        let $oks :=
                for $pubUri in tgclient:getAggregatedUris($tguri, $rdfstoreUrl)
                    let $meta := tgclient:getMeta($pubUri, $sid, $tgcrudUrl),
                        $targetUri := if($meta//tgmd:textgridUri/text()) then substring-before(tgclient:remove-prefix($meta//tgmd:textgridUri/text()), '.') || ".xml" else "0"|| substring-after($pubUri, ":") ||".xml",
                        $storeMetadata :=
                                    if (starts-with($meta//tgmd:format/string(), 'image'))
                                    then ()
                                    else
                                        xmldb:store($targetPath || "/meta", $targetUri, $meta, "text/xml"),
                        $addImageToListOfPublishedImages := (if (starts-with($meta//tgmd:format/string(), 'image') and (doc('/sade-projects/' || $project ||'/images.xml')//object/@uri) != $meta//tgmd:textgridUri/string() ) then (
                            update insert <object type="{$meta//tgmd:format/string()}" uri="{$meta//tgmd:textgridUri/string()}" title="{$meta//tgmd:title/string()}"/> into doc('/sade-projects/' || $project ||'/images.xml')/navigation
                                ) else ())
                    return
                        switch ( string($meta//tgmd:format) )
                            case "text/xml"
                                return
                                    let $data := try {tgclient:getData($pubUri, $sid, $tgcrudUrl) }
                                                    catch * { <error uri="{$pubUri}">{concat($err:code, ": ", $err:description)}</error> }
                                    return
                                        if(starts-with($meta//tgmd:title, "Relax"))
                                        then xmldb:store(concat($targetPath, "/data"), $targetUri || ".rng", $data, "text/xml")
                                        else
                                    let $store := (
                                                    try { xmldb:store(concat($targetPath, "/data"), $targetUri, $data, "text/xml") }
                                                    catch * { concat($err:code, ": ", $err:description) }
    (:                                                xmldb:store(concat($targetPath, "/data"), $targetUri, $data, "text/xml"):)
                                                  )
                                    let $render := if (matches( string($meta//tgmd:title), "Notizbuch\s" )) then (
                                                    try { tgconnect:prerender($data, $targetPath, substring-before($targetUri, '.xml')) }
                                                    catch * { <error uri="{$pubUri}">{concat($err:code, ": ", $err:description)}</error> }
                                                    ) else ()
                                    return
                                        $store
                            case "text/xml+xslt"
                                return
                                    let $data := tgclient:getData($pubUri, $sid, $tgcrudUrl)
                                    return xmldb:store(concat($targetPath, "/data"), $targetUri, $data, "text/xml")
                            case "text/linkeditorlinkedfile"
                                return
                                    let $data := try { tgclient:getData($pubUri, $sid, $tgcrudUrl) }
                                                catch * { <error>{$err:code || ": " || $err:description}</error> }
                                    return try { xmldb:store($targetPath || '/tile', $targetUri, $data) }
                                            catch * { xmldb:store($targetPath || '/tile', $targetUri, <error uri="{$pubUri}">{$err:code || ": " || $err:description}</error>) }
                            case "text/tg.inputform+rdf+xml"
                                return
                                    let $data := tgclient:getData($pubUri, $sid, $tgcrudUrl)
                                    return xmldb:store(concat($targetPath, "/rdf"), $targetUri, $data, "text/xml")
                            case "text/tg.aggregation+xml"
                                return
                                    let $data := tgclient:getData($pubUri, $sid, $tgcrudUrl)
                                    return xmldb:store(concat($targetPath, "/agg"), $targetUri, $data, "text/xml")
                            case "text/plain"
                                return
                                  if( $meta//tgmd:title/text() = "synonyms.txt" )
                                    then
                                      let $text := tgclient:getData($pubUri, $sid, $tgcrudUrl)
                                      let $base64 := xs:base64Binary(util:base64-encode($text))
                                      let $path := "/var/lib/textgrid/"
                                      return
                                        if(  file:is-directory($path) and file:is-writeable($path))
                                        then
                                          let $do := file:serialize-binary( $base64, $path || "synonyms.txt"  ),
                                                $reindex := xmldb:reindex( $targetPath || "/data" )
                                          return
                                          if($do = true()) then $path || "synonyms.txt" else <error uri="{$pubUri}"> could not serialize binary </error>
                                        else <error uri="{$pubUri}"> path not available or not writeable </error>
                                    else <error uri="{$pubUri}"> unknown plain text document </error>
                            default return <error uri="{$pubUri}"> tgmd:format not supported </error>

        return
            if(starts-with($oks, "/db/sade-projects/") or starts-with($oks, "/var/lib/textgrid") )
            then
                if( exists( doc($oks)//error ) )
                then <error uri="{$oks}">level 2: { doc($oks)/error//text()} </error>
                else <div>
                        <ok>{$oks}</ok>
                          {
                            (: validation fails on autodeploy and destroys the whole process :)
                             if( $login = true() ) then () else
                              let $instance := doc( $oks )
                              let $grammar :=  $instance/processing-instruction()[contains(string(.), "http://relaxng.org/ns/structure/1.0")]
                                              /substring-after(substring-before(., '" type'), "textgrid:")
                              let $relaxNGPath :=  $targetPath || "/data/" || $grammar || ".xml.rng"
                              return
                                  if( ($grammar = "" ) or not($instance//tei:TEI) or not( doc-available( $relaxNGPath ) )) then () else
                                      validation:validate-report($instance, doc($relaxNGPath))
(:                                    $grammar:)
                                  }
                    </div>
            else <error>level 1: {$oks} </error>
    else
        <error>error authenticating for {$user} - {$password} on {$targetPath}</error>
(:        tgconnect:error401:)
};

(: does not work with restxq-xquery impl :)
(: declare function tgconnect:error401() {
  <rest:response>
    <http:response status="401" message="wrong user or password">
      <http:header name="Content-Language" value="en"/>
      <http:header name="Content-Type" value="text/html; charset=utf-8"/>
    </http:response>
  </rest:response>
};:)

declare function tgconnect:createEntryPoint($uri as xs:string, $path as xs:string) {
(:    let $epUri := concat(tgclient:remove-prefix($uri), ".ep.xml"):)
(:    return xmldb:store($path, $epUri, <entrypoint>{$uri}</entrypoint>, "text/xml"):)
 true()
};

declare function tgconnect:buildmenu($project as xs:string, $targetPath as xs:string, $template as xs:string) {
let $nav := tgmenu:init($project, $targetPath),
    $egal := xmldb:store('/sade-projects/' || $project, '/navigation-tg.xml', $nav, "text/xml"),
    $last := transform:transform($nav, doc('/sade-projects/' || $project || '/xslt/tg-menu.xslt'), ()),
    $egal := xmldb:store('/sade-projects/' || $project, '/navigation-' || $template || '.xml', $last, "text/xml")
return " menu: ok"
};

declare function tgconnect:buildstats($project as xs:string) {

    let $path := '/sade-projects/' || $project
    let $coll := collection($path || '/data/xml/data')
    let $doc := doc($path || '/stats.xml')

    let $date := current-dateTime()

    let $words :=
     sum(for $doc in $coll
          return count(tokenize(string($doc), '\W+')[. != '']))

    let $doc :=
        <stats>
            {$doc/stats/publ}
            <publ>
                <date>{$date}</date>
                <words>{$words}</words>
            </publ>
        </stats>

    let $tmp := xmldb:store($path, 'stats.xml', $doc, 'text/xml')
    return ' stats: ok'

};
declare function tgconnect:store($targetPath, $type, $targetUri, $data){
((if(doc-available(concat($targetPath, '/', $type, '/', $targetUri))) then xmldb:remove(concat($targetPath, '/', $type), $targetUri) else () ),
xmldb:store(concat($targetPath, '/', $type), $targetUri, $data, "text/xml"))
};

declare function tgconnect:prerender($data, $path, $uri){
if(starts-with($data//tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/string(.), 'Notizbuch'))
then
    let $collection-uri := $path||'/xhtml/'||$uri,
        $remove:= if(xmldb:collection-available($collection-uri))
                    then xmldb:remove($collection-uri)
                    else (),
        $create := xmldb:create-collection($path||'/xhtml', $uri),
        $data := $data//tei:TEI,
        $nb := tokenize($data//tei:sourceDoc/string(@n), '_')[last()],
        $count := count( $data/tei:sourceDoc/tei:surface ),
        $page :=    for $sf at $pos in
($data/tei:sourceDoc/tei:surface,
$data/tei:sourceDoc//tei:surface[@type="additional"],
$data/tei:sourceDoc//tei:surface[@type="clipping"])
(:                    where $sf/string(@n) = "77r":)
                    return
                        let $sfcode := $nb||'_'||$sf/string(@n),
(:   we are removing surface extraction because the loss of context is to heavy.
this should decrease the performance! sorry!
                            $extract := try{fontaneSfEx:extract( $data, $sf/string(@n) )}catch * {<error> sfExtract: {$err:code}: {$err:description}{console:log($sfcode||': '|| $err:code||': '||$err:description)}</error>},
:)                            $transfo := try{fontaneTransfo:magic($sf)}catch * {<error>Magic: {$err:code}: {$err:description}{console:log( $sfcode||': '|| $err:code||': '||$err:description)}</error>},
                            $store :=   try{ xmldb:store( $collection-uri, $sf/string(@n)||'.xml', <xhtml:body>{$transfo}</xhtml:body>) }
                                        catch * {<error>Store: {$err:code}: {$err:description}{console:log($sfcode||': '|| $err:code||': '||$err:description)}</error>}
                        return
                            (local:progress($uri, $sf/string(@n), $pos div $count)
                            , if($transfo/error) then console:log($transfo) else ()
                            )
    return
        let $clearProgress := local:progress('done', 'clear', 0),
            $toc := xmldb:store( $collection-uri, 'toc.xml', fontaneTransfo:toc($data) ),
            $renderXml := xmldb:store( $collection-uri, 'xml.xml', local:renderXmlCode($data) )
        return 'ok'
else 'ok'
};

declare function local:renderXmlCode($node) {
element xhtml:div {
                        attribute class {'teixmlview'},
                        element xhtml:pre {
                            element xhtml:code {
                                attribute class {'html'},
                                replace(replace(replace(serialize($node), '<!--.*?-->|^[\s|\t]+?\n', ''), '^\s+?\n', ''), '\n\s+\n+?', '')
                            }
                        }
                        }
};

declare function local:progress($uri as xs:string, $page as xs:string, $value as xs:float){
let $new := '{"uri":"'||$uri||'", "page":"'||$page||'","progress":"'||round($value * 100)||'"}'
return
    xmldb:store( '/db/apps/textgrid-connect/resources/js/', 'progress.json', $new, 'text/javascript' )
};

declare function local:copy-filter-elements($element, $element-name as xs:string*) as item() {

(: untyped variables only: PI is not an element, element is not PI, ... :)

if($element instance of processing-instruction()) then
    let $wrapper :=  <wrap>{$element}</wrap>
    return
        $wrapper/processing-instruction()
else
   element {node-name($element) }
         { $element/@*,
           for $child in $element/node()[not(name(.)=$element-name)]
              return if ($child instance of element())
                then local:copy-filter-elements($child,$element-name)
                else $child
       }
 };
